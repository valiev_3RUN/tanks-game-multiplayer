#include "game.h"


game_t::game_t(game_mode_t game_mode) {
    this->game_mode = game_mode;
}



game_t::~game_t() {
//    if (this->tank_player1 != nullptr) delete this->tank_player1;
//    if (this->tank_player2 != nullptr) delete this->tank_player2;
//    if (this->map != nullptr)          delete this->map;
}

void game_t::createTexts() {
    static Font font;
    font.loadFromFile("./Sources/font.TTF");
    text = *(new Text("", font, 20));
//    text.setColor(Color::Red);
    text.setStyle(Text::Bold);
    text.setPosition(this->map->map_tail_image.getSize().y * this->map->size_square_map + 2.f,
                     this->map->map_tail_image.getSize().y);
}

void game_t::createMap(const string map_path, size_t size_square_map) {
    this->map = new map_t(size_square_map, map_path);
    this->size_square_map = size_square_map;
    this->createTexts();
}

void game_t::createPlayer1(const string tank_player1_path) {
    assert(map != nullptr);
    this->tank_player1 = new tank_t(tank_player1_path, tank_t::PLAYER_1);
    this->tank_player1->setPosition(map->map_tail_image.getSize().y + 2.f, map->map_tail_image.getSize().y * (this->size_square_map - 2) + 2.f);
}

void game_t::createPlayer2(const string tank_player2_path) {
    assert(map != nullptr);
    assert(game_mode == this->MULTIPLAYER);
    this->tank_player2 = new tank_t(tank_player2_path, tank_t::PLAYER_2);
    this->tank_player2->setDirection(entity_t::DOWN);
    this->tank_player2->setPosition(map->map_tail_image.getSize().y * (this->size_square_map - 1) - 2.f,
                                    map->map_tail_image.getSize().y + this->tank_player2->getImage().getSize().y + 2.f);

}

void game_t::createBotsTank(const string bot_tank_path, uint8_t count_bots) {
    assert(map != nullptr);
    this->count_bots = count_bots;
    for (uint8_t i = 0; i < count_bots; ++i) {
        this->bots.push_back(new tank_t(bot_tank_path, tank_t::BOT));
        this->bots[i]->setDirection(entity_t::UP);
        this->bots[i]->setPosition(this->map->map_tail_image.getSize().y*2 - 2.f,this->map->map_tail_image.getSize().y*2 - 2.f);
    }
}

void game_t::moveBots() {
    direction_t arr_dir[] = {direction_t::RIGHT, direction_t::LEFT, direction_t::UP, direction_t::DOWN};
    static uint8_t index_dir = 0;
    for (tank_t* bot : this->bots) {
        controllingTank(*bot, arr_dir[index_dir], 0.1f);
        index_dir = rand() % static_cast<int>(sizeof(arr_dir)/sizeof(arr_dir[0]));
        if (clock_shot.getElapsedTime().asMilliseconds() > 700) {
            bot->makeShot();
        }
    }
    if (clock_shot.getElapsedTime().asMilliseconds() > 700) clock_shot.restart();
}

vector<tank_t*>& game_t::getTanksOfBots() {
    return this->bots;
}

tank_t* game_t::getTankOfPlayer1() {
    return this->tank_player1;
}

tank_t* game_t::getTankOfPlayer2() {
    return this->tank_player2;
}

void game_t::update_tanks(RenderWindow& window) {
    list<thread> threads;
    const uint8_t damage = 1;
    auto move_bullet_up = [] (tank_t& tank, float bullet_x, float bullet_y, float bullet_hight) {
        float tank_x       = tank.getPosition().x,
              tank_y       = tank.getPosition().y,
              tank_hight   = tank.getHight();

        switch (tank.getDirection())
        {
        case direction_t::DOWN:
            if (bullet_y <= tank_y && bullet_y >= (tank_y - tank_hight) &&  bullet_x >= (tank_x - tank_hight - bullet_hight) && bullet_x <= tank_x) {
                tank.setDamage(damage);
            }
        break;
        case direction_t::UP:
            if (bullet_y <= (tank_y + tank_hight) && bullet_y >= (tank_y) && (bullet_x + bullet_hight) >= tank_x && bullet_x <= (tank_x + tank_hight)) {
               tank.setDamage(damage);
            }
        break;
        case direction_t::RIGHT:
            if (bullet_y <= (tank_y + tank_hight) && bullet_y >= (tank_y-bullet_hight) && (bullet_x + tank_hight + bullet_hight) >= tank_x && bullet_x <= (tank_x)) {
                tank.setDamage(damage);
            }
        break;
        case direction_t::LEFT:
            if (bullet_y <= tank_y && bullet_y >= (tank_y-tank_hight - bullet_hight) && (bullet_x) >= (tank_x - bullet_hight) && tank_x <= (tank_x + tank_hight)) {
               tank.setDamage(damage);
            }
        break;
        }
    };

    auto move_bullet_down = [] (tank_t& tank, float bullet_x, float bullet_y, float bullet_hight) {
        float tank_x       = tank.getPosition().x,
              tank_y       = tank.getPosition().y,
              tank_hight   = tank.getHight();

        switch (tank.getDirection())
        {
        case direction_t::DOWN:
            if (bullet_y <= (tank_y + tank_hight) && bullet_y >= (tank_y - tank_hight) &&  (bullet_x+ tank_hight) >= (tank_x) && bullet_x <=(tank_x + bullet_hight)) {
                tank.setDamage(damage);
            }
        break;
        case direction_t::UP:
            if (bullet_y <= (tank_y + tank_hight + bullet_hight) && bullet_y >= (tank_y) && (bullet_x) >= tank_x && bullet_x <= (tank_x + tank_hight + bullet_hight)) {
               tank.setDamage(damage);
            }
        break;
        case direction_t::RIGHT:
            if (bullet_y <= (tank_y + tank_hight + bullet_hight) && (bullet_y) >= (tank_y) && (bullet_x) >= (tank_x - tank_hight)  && bullet_x <= (tank_x + bullet_hight)) {
                tank.setDamage(damage);
            }
        break;
        case direction_t::LEFT:
            if (bullet_y <= (tank_y + tank_hight) && bullet_y >= (tank_y - tank_hight) && (bullet_x) >= (tank_x) && bullet_x <= (tank_x + tank_hight + bullet_hight)) {
               tank.setDamage(damage);
            }
        break;
        }
    };

    auto move_bullet_right = [] (tank_t& tank, float bullet_x, float bullet_y, float bullet_hight) {
        float tank_x       = tank.getPosition().x,
              tank_y       = tank.getPosition().y,
              tank_hight   = tank.getHight();

        switch (tank.getDirection())
        {
        case direction_t::DOWN:
            if (bullet_y <= (tank_y) && bullet_y >= (tank_y - tank_hight - bullet_hight) &&  (bullet_x+ tank_hight) >= (tank_x) && bullet_x <=(tank_x + bullet_hight)) {
                tank.setDamage(damage);
            }
        break;
        case direction_t::UP:
            if (bullet_y >= (tank_y - bullet_hight) && bullet_y <= (tank_y + tank_hight)  && (bullet_x) >= tank_x && bullet_x <= (tank_x + tank_hight + bullet_hight)) {
               tank.setDamage(damage);
            }
        break;
        case direction_t::RIGHT:
            if (bullet_y >= (tank_y - bullet_hight) && bullet_y <= (tank_y + tank_hight) && (bullet_x) >= (tank_x - tank_hight)  && bullet_x <= (tank_x + bullet_hight)) {
                tank.setDamage(damage);
            }
        break;
        case direction_t::LEFT:
            if (bullet_y >= (tank_y - tank_hight - bullet_hight) && bullet_y <= tank_y  && (bullet_x) >= (tank_x) && bullet_x <= (tank_x + tank_hight + bullet_hight)) {
               tank.setDamage(damage);
            }
        break;
        }
    };

    auto move_bullet_left = [] (tank_t& tank, float bullet_x, float bullet_y, float bullet_hight) {
        float tank_x       = tank.getPosition().x,
              tank_y       = tank.getPosition().y,
              tank_hight   = tank.getHight();

        switch (tank.getDirection())
        {
        case direction_t::DOWN:
            if (bullet_y <= (tank_y + bullet_hight) && bullet_y >= (tank_y - tank_hight) &&  (bullet_x) <= (tank_x) && bullet_x >= (tank_x - tank_hight - bullet_hight)) {
                tank.setDamage(damage);
            }
        break;
        case direction_t::UP:
            if (bullet_y <= (tank_y + tank_hight + bullet_hight) && bullet_y >= (tank_y) && (bullet_x) <= (tank_x + tank_hight) && bullet_x >= (tank_x - bullet_hight)) {
               tank.setDamage(damage);
            }
        break;
        case direction_t::RIGHT:
            if (bullet_y <= (tank_y + tank_hight + bullet_hight) && (bullet_y) >= (tank_y) && (bullet_x) >= (tank_x - tank_hight - bullet_hight)  && bullet_x <= (tank_x)) {
                tank.setDamage(damage);
            }
        break;
        case direction_t::LEFT:
            if (bullet_y <= (tank_y + bullet_hight) && bullet_y >= (tank_y - tank_hight) && (bullet_x) >= (tank_x - bullet_hight) && bullet_x <= (tank_x + tank_hight)) {
               tank.setDamage(damage);
            }
        break;
        }
    };

    auto move_bullet = [move_bullet_up, move_bullet_down, move_bullet_right, move_bullet_left](game_t& game, tank_t& tank, RenderWindow& window) {
        uint16_t it = 0;
        float speed_shot = 0.4f;
        for (bullet_t &shot : tank.getListShots()) {
             window.draw(shot.getSprite());
             float bullet_x = shot.getPosition().x,
                   bullet_y = shot.getPosition().y,
                   bullet_hight = shot.getHight();
             switch (shot.getDirection()) {
                case direction_t::UP:
                    shot.move(0, -speed_shot*game.time);
                    if (game.tank_player1 != nullptr) {
//                        if (game.tank_player1->getTypeEntity() != entity_t::PLAYER_1) {
                            move_bullet_up(*game.tank_player1, bullet_x, bullet_y, bullet_hight);
//                        }
                    }
                    if (game.tank_player2 != nullptr) {
//                        if (game.tank_player2->getTypeEntity() != entity_t::PLAYER_2) {
                            move_bullet_up(*game.tank_player2, bullet_x, bullet_y, bullet_hight);
//                        }
                    }
                    if (tank.getTypeEntity() != entity_t::BOT) {
                        for (tank_t* bot : game.bots) {
                            move_bullet_up(*bot, bullet_x, bullet_y, bullet_hight);
                        }
                    }
                break;

                case direction_t::DOWN:
                    shot.move(0, speed_shot*game.time);
                    if (game.tank_player1 != nullptr) {
//                        if (game.tank_player1->getTypeEntity() != entity_t::PLAYER_1) {
                            move_bullet_down(*game.tank_player1, bullet_x, bullet_y, bullet_hight);
//                        }
                    }
                    if (game.tank_player2 != nullptr) {
//                        if (game.tank_player2->getTypeEntity() != entity_t::PLAYER_2) {
                            move_bullet_down(*game.tank_player2, bullet_x, bullet_y, bullet_hight);
//                        }
                    }
                    if (tank.getTypeEntity() != entity_t::BOT) {
                        for (tank_t* bot : game.bots) {
                            move_bullet_down(*bot, bullet_x, bullet_y, bullet_hight);
                        }
                    }
                break;

                case direction_t::RIGHT:
                    shot.move( speed_shot*game.time, 0);
                    if (game.tank_player1 != nullptr) {
//                        if (game.tank_player1->getTypeEntity() != entity_t::PLAYER_1) {
                            move_bullet_right(*game.tank_player1, bullet_x, bullet_y, bullet_hight);
                        }
//                    }
                    if (game.tank_player2 != nullptr) {
//                        if (game.tank_player2->getTypeEntity() != entity_t::PLAYER_2) {
                            move_bullet_right(*game.tank_player2, bullet_x, bullet_y, bullet_hight);
//                        }
                    }
                    if (tank.getTypeEntity() != entity_t::BOT) {
                        for (tank_t* bot : game.bots) {
                            move_bullet_right(*bot, bullet_x, bullet_y, bullet_hight);
                        }
                    }
                break;
                case direction_t::LEFT:
                    shot.move(-speed_shot*game.time, 0);
                    if (game.tank_player1 != nullptr) {
//                        if (game.tank_player1->getTypeEntity() != entity_t::PLAYER_1) {
                            move_bullet_left(*game.tank_player1, bullet_x, bullet_y, bullet_hight);
//                        }
                    }
                    if (game.tank_player2 != nullptr) {
//                        if (game.tank_player2->getTypeEntity() != entity_t::PLAYER_2) {
                            move_bullet_left(*game.tank_player2, bullet_x, bullet_y, bullet_hight);
//                        }
                    }
                    if (tank.getTypeEntity() != entity_t::BOT) {
                        for (tank_t* bot : game.bots) {
                            move_bullet_left(*bot, bullet_x, bullet_y, bullet_hight);
                        }
                    }
                break;
             }

            if (!game.isHaveResolutionToMove(shot, shot.getDirection())) {
                tank.getListShots().erase(tank.getListShots().begin() + it);
                break;
            }
            it++;
        }
    };


    if (this->tank_player1 != nullptr) {
        move_bullet(*this, *(this->tank_player1), window);
        window.draw(this->tank_player1->getSprite());
        if (!this->tank_player1->getHelth()) {
            delete this->tank_player1;
            this->tank_player1 = nullptr;
        }
    }

    if (game_mode == this->MULTIPLAYER && this->tank_player2 != nullptr)  {
        move_bullet(*this, *(this->tank_player2), window);
        window.draw(this->tank_player2->getSprite());
        if (!this->tank_player2->getHelth()) {
            delete this->tank_player2;
            this->tank_player2 = nullptr;
        }
    }

    for (uint8_t i = 0; i < this->bots.size(); ++i) {
        if (!bots[i]->getHelth()) {
            delete bots[i];
            bots.erase(bots.begin() + i);
            if (i - 1 > 0) --i;
        }
    }

    for (tank_t* bot : this->bots) {
        move_bullet(*this, *bot, window);
        window.draw(bot->getSprite());
    }

}

void game_t::update_texts(RenderWindow& window) {
    std::string msg = string("Helth of player1: ") + ( this->tank_player1 != nullptr ? std::to_string(this->tank_player1->getHelth()) : "killed") +
                      string("\n\nHelth of Player2: ") + (this->tank_player2 != nullptr ? std::to_string(this->tank_player2->getHelth()) : "killed");
    text.setString(msg);
    window.draw(text);
}

void game_t::update_times() {
    this->time = this->clock.getElapsedTime().asMicroseconds();
    this->clock.restart();
    this->time /= 1000;
    srand(static_cast<unsigned int>(std::time(nullptr)));
}

void game_t::update(RenderWindow& window) {
    window.clear(Color(128,106,89));
    this->update_times();
    this->update_texts(window);
    this->map->draw(window);
    this->update_tanks(window);
    window.display();
}

bool game_t::isHaveResolutionToMove(entity_t& entity, direction_t dir) {
    assert(map != nullptr);
    entity.setDirection(dir);
    float   entity_y   = entity.getSprite().getPosition().y,
            entity_x   = entity.getSprite().getPosition().x,
            map_w_h  = map->getSizeTailImage().y,
            hero_w_h = entity.getImage().getSize().y;
    vector<Vector2f> vec = map->coordinates_obstacles;
    bool isHaveResolutionToMove = true;
    switch (dir) {
        case entity_t::UP:
            isHaveResolutionToMove = true;
            for (uint16_t i = 0; i < vec.size(); ++ i) {
                if (entity_y >= (vec[i].y + map_w_h) && entity_y <= (vec[i].y + map_w_h + 2) && entity_x>= (vec[i].x-hero_w_h) && entity_x <= (vec[i].x+map_w_h))
                isHaveResolutionToMove = false;
            }
        break;

        case entity_t::DOWN:
            isHaveResolutionToMove = true;
            for (uint16_t i = 0; i < vec.size(); ++ i) {
                if (entity_y >= (vec[i].y - 2) && entity_y <= (vec[i].y + map_w_h) && entity_x>= (vec[i].x) && entity_x <= (vec[i].x+map_w_h+hero_w_h))
                isHaveResolutionToMove = false;
            }
        break;

        case entity_t::LEFT:
            isHaveResolutionToMove = true;
            for (uint16_t i = 0; i < vec.size(); ++ i) {
                if (entity_y >= (vec[i].y) && entity_y <= (vec[i].y + map_w_h + hero_w_h) && entity_x>= (vec[i].x) && entity_x <= (vec[i].x+map_w_h + 2))
                isHaveResolutionToMove = false;
            }
        break;

        case entity_t::RIGHT:
        isHaveResolutionToMove = true;
        for (uint16_t i = 0; i < vec.size(); ++ i) {
            if (entity_y >= (vec[i].y - hero_w_h) && entity_y <= (vec[i].y + map_w_h) && entity_x>= (vec[i].x - 2) && entity_x <= (vec[i].x+map_w_h))
            isHaveResolutionToMove = false;
        }
        break;
    }
    if (entity_y <= map_w_h || entity_x <= map_w_h) return false;
    return isHaveResolutionToMove;
}

bool game_t::controllingTank(tank_t& tank, direction_t dir, float speed) {
    tank.setDirection(dir);
    switch (dir) {
    case direction_t::RIGHT:
        if (this->isHaveResolutionToMove(tank, dir)) {
            tank.move(speed*time, 0);
            return true;
        }
        break;
    case direction_t::LEFT:
        if (this->isHaveResolutionToMove(tank, dir)) {
            tank.move(-speed*time, 0);
            return true;
        }
        break;
    case direction_t::UP:
        if (this->isHaveResolutionToMove(tank, dir)) {
            tank.move(0, -speed*time);
            return true;
        }
        break;
    case direction_t::DOWN:
        if (this->isHaveResolutionToMove(tank, dir)) {
            tank.move(0, speed*time);
            return true;
        }
        break;
    }
    return false;
}

void game_t::isKeyboardControlling(tank_t* tank, float speed, Packet& packet) {
        if (tank != nullptr) {
            if (tank == this->tank_player2) {
                if ((Keyboard::isKeyPressed(Keyboard::Right))) {
                    this->controllingTank(*tank, direction_t::RIGHT, speed);
                    packet << tank->getPosition().x << tank->getPosition().y;
                } else

                if (Keyboard::isKeyPressed(Keyboard::Left)) {
                    this->controllingTank(*tank, direction_t::LEFT, speed);
                    packet << tank->getPosition().x << tank->getPosition().y;
                } else

                if (Keyboard::isKeyPressed(Keyboard::Up) ) {
                    this->controllingTank(*tank, direction_t::UP, speed);
                    packet << tank->getPosition().x << tank->getPosition().y;
                } else

                if (Keyboard::isKeyPressed(Keyboard::Down)) {
                    this->controllingTank(*tank, direction_t::DOWN, speed);
                    packet << tank->getPosition().x << tank->getPosition().y;
                }
            } else if (tank == this->tank_player1){
                if (Keyboard::isKeyPressed(Keyboard::D)) {
                   this->controllingTank(*tank, direction_t::RIGHT, speed);
                   packet << tank->getPosition().x << tank->getPosition().y;
                } else

                if (Keyboard::isKeyPressed(Keyboard::A)) {
                    this->controllingTank(*tank, direction_t::LEFT, speed);
                    packet << tank->getPosition().x << tank->getPosition().y;
                } else

                if (Keyboard::isKeyPressed(Keyboard::W)) {
                    this->controllingTank(*tank, direction_t::UP, speed);
                    packet << tank->getPosition().x << tank->getPosition().y;
                } else

                if ( Keyboard::isKeyPressed(Keyboard::S)) {
                    this->controllingTank(*tank, direction_t::DOWN, speed);
                    packet << tank->getPosition().x << tank->getPosition().y;
                }
            }

        }



//    if (tank != nullptr) {
//        if ((Keyboard::isKeyPressed(Keyboard::Right) && this->current_tp == SERVER) || (Keyboard::isKeyPressed(Keyboard::D) && this->current_tp == CLIENT)) {
//            this->controllingTank(*tank, direction_t::RIGHT, speed);
//        } else

//        if ((Keyboard::isKeyPressed(Keyboard::Left) && this->current_tp == SERVER) || (Keyboard::isKeyPressed(Keyboard::A) && this->current_tp == CLIENT)) {
//            this->controllingTank(*tank, direction_t::LEFT, speed);
//        } else

//        if ((Keyboard::isKeyPressed(Keyboard::Up) && this->current_tp == SERVER) || (Keyboard::isKeyPressed(Keyboard::W) && this->current_tp == CLIENT)) {
//            this->controllingTank(*tank, direction_t::UP, speed);
//        } else

//        if ((Keyboard::isKeyPressed(Keyboard::Down) && this->current_tp == SERVER) || (Keyboard::isKeyPressed(Keyboard::S) && this->current_tp == CLIENT)) {
//            this->controllingTank(*tank, direction_t::DOWN, speed);
//        }
//    }

}
