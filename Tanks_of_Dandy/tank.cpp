#include "tank.h"

entity_t::~entity_t() {}

entity_t::entity_t(const string path) : helth(100) {
    this->obj_image.loadFromFile(path);
    this->obj_image.createMaskFromColor(Color(0,0,0));
    this->obj_texture.loadFromImage(this->obj_image);
    this->obj_sprite.setTexture(this->obj_texture);
}

void entity_t::move(const float &x, const float &y) {
    this->obj_sprite.move(x, y);
}


Vector2f entity_t::getPosition()  const {
    return this->obj_sprite.getPosition();
}

void entity_t::setPosition(const float& x,const  float& y)  {
     this->obj_sprite.setPosition(x, y);
}

uint8_t& entity_t::getHelth() {
    return this->helth;
}

Sprite& entity_t::getSprite() {
    return this->obj_sprite;
}

Image& entity_t::getImage() {
    return  this->obj_image;
}

uint8_t entity_t::getHight() {
    return static_cast<uint8_t>(this->getSprite().getTextureRect().height);
}

entity_t::type_entity_t entity_t::getTypeEntity() {
    return this->type_entity;
}


void entity_t::setDirection(direction_t direction) {
    float x = this->getPosition().x,
          y = this->getPosition().y;
    switch (direction)
    {
        case direction_t::RIGHT:
            switch (static_cast<int>(this->getSprite().getRotation()))
            {
                case 0:
                    this->getSprite().setRotation(90);
                    this->setPosition(x + obj_image.getSize().y, y);
                break;
                case 180:
                    this->getSprite().setRotation(90);
                    this->setPosition(x, y - obj_image.getSize().y);
                break;
                case 270:
                    this->getSprite().setRotation(90);
                    this->setPosition(x + obj_image.getSize().y, y - obj_image.getSize().y);
                break;
            }
            this->current_dir = direction_t::RIGHT;
        break;
        case direction_t::LEFT :
            switch (static_cast<int>(this->getSprite().getRotation()))
            {
                case 0:
                    this->getSprite().setRotation(270);
                    this->setPosition(x, y + obj_image.getSize().y);
                break;
                case 90:
                    this->getSprite().setRotation(270);
                    this->setPosition(x - obj_image.getSize().y, y + obj_image.getSize().y);
                break;
                case 180 :
                    this->getSprite().setRotation(270);
                    this->setPosition(x - obj_image.getSize().y, y);
                break;
            }
            this->current_dir = direction_t::LEFT;
        break;
        case direction_t::UP :
            switch (static_cast<int>(this->getSprite().getRotation()))
            {
                case 90:
                    this->getSprite().setRotation(0);
                    this->setPosition(x - obj_image.getSize().y, y);
                break;
                case 180:
                    this->getSprite().setRotation(0);
                    this->setPosition(x - obj_image.getSize().y, y - obj_image.getSize().y);
                break;
                case 270 :
                    this->getSprite().setRotation(0);
                    this->setPosition(x, y - obj_image.getSize().y);
                break;
            }
            this->current_dir = direction_t::UP;

        break;
        case direction_t::DOWN :
            switch (static_cast<int>(this->getSprite().getRotation()))
            {
                case 0:
                    this->getSprite().setRotation(180);
                    this->setPosition(x + obj_image.getSize().y, y + obj_image.getSize().y);
                break;
                case 90:
                    this->getSprite().setRotation(180);
                    this->setPosition(x , y + obj_image.getSize().y);
                break;
                case 270 :
                    this->getSprite().setRotation(180);
                    this->setPosition(x + obj_image.getSize().y, y);
                break;
            }
            this->current_dir = direction_t::DOWN;
        break;
    }
}

entity_t::direction_t entity_t::getDirection() {

    switch (static_cast<uint16_t>(this->getSprite().getRotation())) {
    case 0:
        return direction_t::UP;
    case 90:
        return direction_t::RIGHT;
    case 180:
        return direction_t::DOWN;
    case 270:
        return direction_t::LEFT;
    }
    return this->current_dir;
}

tank_t::tank_t(const string path, type_entity_t type_entity) : entity_t(path) {
    this->type_entity = type_entity;
}

void tank_t::setDamage(uint8_t damage) {
    if (this->helth > 0)
        this->helth -= damage;
}

vector<bullet_t>& tank_t::getListShots() {
    return this->list_shots;
}

void tank_t::makeShot() {
    static bullet_t bullet("./Sources/bullet.png");
    list_shots.push_back(bullet);
    const float bullet_height = list_shots.back().getSprite().getTextureRect().height;
    const float tank_height   = this->obj_sprite.getTextureRect().height;
    const float size = 15;
    switch (this->getDirection()) {
    case UP:
        list_shots.back().setPosition(this->getPosition().x + (tank_height - bullet_height) / 2, this->getPosition().y - size);
        break;
    case DOWN:
        list_shots.back().setPosition(this->getPosition().x - (tank_height + bullet_height) / 2, this->getPosition().y + size);
        break;
    case RIGHT:
        list_shots.back().setPosition(this->getPosition().x + size, this->getPosition().y + (tank_height - bullet_height) / 2);
        break;
    case LEFT:
        list_shots.back().setPosition(this->getPosition().x - size, this->getPosition().y - (tank_height + bullet_height) / 2);
        break;
    }
    list_shots.back().setDirection(this->getDirection());
}

tank_t::~tank_t() {}

bullet_t::bullet_t(const string path) : entity_t(path) {}
bullet_t::bullet_t()  {}
bullet_t::~bullet_t() {}
