#include "game.h"
using namespace std;

using direction_t = entity_t::direction_t;


static sf::TcpListener listener;
static sf::TcpSocket socket;
static sf::IpAddress server;

void runTcpServer(unsigned short port)
{
    // Create a server socket to accept new connections

    // Listen to the given port for incoming connections
    if (listener.listen(port) != sf::Socket::Done)
        return;
    std::cout << "Server is listening to port " << port << ", waiting for connections... " << std::endl;

    // Wait for a connection
    if (listener.accept(socket) != sf::Socket::Done)
        return ;
    std::cout << "Client connected: " << socket.getRemoteAddress() << std::endl;
    socket.setBlocking(0);
}


////////////////////////////////////////////////////////////
/// Create a client, connect it to a server, display the
/// welcome message and send an answer.
///
////////////////////////////////////////////////////////////
void runTcpClient(unsigned short port)
{
    // Ask for the server address
    do
    {
        std::cout << "Type the address or name of the server to connect to: ";
        std::cin  >> server;
    }
    while (server == sf::IpAddress::None);

    // Create a socket for communicating with the server

    // Connect to the server
    if (socket.connect(server, port) != sf::Socket::Done)
        return;
    std::cout << "Connected to server " << server << std::endl;
    socket.setBlocking(0);
    return;
}


int main()
{

    RenderWindow window(VideoMode(775, 545), "Tanks of dendy multiplayer");
    list<thread> threads;
    Clock clock, past_shot, past_shot_enemy;
    using dir_t = enum tank_t::direction_t;
    game_t game(game_t::MULTIPLAYER);
           game.createMap("./Sources/map.png", 17);
           game.createPlayer1("./Sources/player_1_little.png");
           game.createPlayer2("./Sources/player_2_little.png");
           game.createBotsTank("./Sources/player_2_little.png", 0);

        string choose;
        cout << "Enter the s/c (server/client):  ";
        cin >> choose;
        if (choose == 's') {
            runTcpServer(50111);
        }
        else {
            runTcpClient(50111);
        }


    while (window.isOpen())
    {
        if (window.hasFocus()) {
            Event event;
            threads.push_back(thread([&game]() { game.moveBots();}));
            Packet packet;
            while (window.pollEvent(event))
            {
                if (event.type == sf::Event::Closed || Keyboard::isKeyPressed(Keyboard::Escape)) window.close();
            }

            tank_t* player1 = game.getTankOfPlayer1();
            tank_t* player2 = game.getTankOfPlayer2();
            bool isKeyPressedPlayer1 = false;
            bool isKeyPressedPlayer2 = false;
            const float speed = 0.1f;
            if (choose == "c"){
                if (player1 != nullptr) {
                    if ((Keyboard::isKeyPressed(Keyboard::Right))) {
                        game.controllingTank(*player1, direction_t::RIGHT, speed);
                        if (Keyboard::isKeyPressed(Keyboard::Space) && past_shot.getElapsedTime().asMilliseconds() > 150) {
                            game.getTankOfPlayer1()->makeShot();
                            past_shot.restart();
                        }
                        isKeyPressedPlayer1 = true;
                    } else
                    if (Keyboard::isKeyPressed(Keyboard::Left)) {
                        game.controllingTank(*player1, direction_t::LEFT, speed);
                        if (Keyboard::isKeyPressed(Keyboard::Space) && past_shot.getElapsedTime().asMilliseconds() > 150) {
                            game.getTankOfPlayer1()->makeShot();
                            past_shot.restart();
                        }
                        isKeyPressedPlayer1 = true;
                    } else

                    if (Keyboard::isKeyPressed(Keyboard::Up) ) {
                        game.controllingTank(*player1, direction_t::UP, speed);
                        if (Keyboard::isKeyPressed(Keyboard::Space) && past_shot.getElapsedTime().asMilliseconds() > 150) {
                            game.getTankOfPlayer1()->makeShot();
                            past_shot.restart();
                        }
                        isKeyPressedPlayer1 = true;
                    } else

                    if (Keyboard::isKeyPressed(Keyboard::Down)) {
                        game.controllingTank(*player1, direction_t::DOWN, speed);
                        if (Keyboard::isKeyPressed(Keyboard::Space) && past_shot.getElapsedTime().asMilliseconds() > 150) {
                            game.getTankOfPlayer1()->makeShot();
                            past_shot.restart();
                        }
                        isKeyPressedPlayer1 = true;                     
                    } else

                    if (Keyboard::isKeyPressed(Keyboard::Space) && past_shot.getElapsedTime().asMilliseconds() > 150) {
                        game.getTankOfPlayer1()->makeShot();
                        past_shot.restart();
                        isKeyPressedPlayer1 = true;
                    } else {
                        isKeyPressedPlayer1 = false;
                    }

                    if (isKeyPressedPlayer1)
                    {
                        float player1_x = game.getTankOfPlayer1()->getPosition().x,
                              player1_y = game.getTankOfPlayer1()->getPosition().y;
                        dir_t current_dir = game.getTankOfPlayer1()->getDirection();
                        uint8_t helth = game.getTankOfPlayer1()->getHelth();
                        bool isShot = game.getTankOfPlayer1()->getListShots().size();
                        packet << player1_x << player1_y << static_cast<uint8_t>(current_dir) << helth <<isShot;
                        socket.send(packet);
                    }
                }

                if (socket.receive(packet) == sf::Socket::Done){
                    float player2_x, player2_y;
                    uint8_t current_dir, helth;
                    bool isShot;
                    packet >> player2_x >> player2_y >> current_dir >> helth >> isShot;
                    game.getTankOfPlayer2()->setPosition(player2_x, player2_y);
                    game.getTankOfPlayer2()->setDirection(static_cast<tank_t::direction_t>(current_dir));
                    game.getTankOfPlayer2()->getHelth() = helth;
                    if (isShot && past_shot_enemy.getElapsedTime().asMilliseconds() > 150) {
                        game.getTankOfPlayer2()->makeShot();
                        past_shot_enemy.restart();
                    }
                }

            } else {
                if (player2 != nullptr) {
                    if ((Keyboard::isKeyPressed(Keyboard::Right))) {
                        game.controllingTank(*player2, direction_t::RIGHT, speed);
                        if (Keyboard::isKeyPressed(Keyboard::Space) && past_shot.getElapsedTime().asMilliseconds() > 150) {
                            game.getTankOfPlayer2()->makeShot();
                            past_shot.restart();
                        }
                        isKeyPressedPlayer2 = true;
                    } else

                    if (Keyboard::isKeyPressed(Keyboard::Left)) {
                        game.controllingTank(*player2, direction_t::LEFT, speed);
                        if (Keyboard::isKeyPressed(Keyboard::Space) && past_shot.getElapsedTime().asMilliseconds() > 150) {
                            game.getTankOfPlayer2()->makeShot();
                            past_shot.restart();
                        }
                        isKeyPressedPlayer2 = true;
                    } else

                    if (Keyboard::isKeyPressed(Keyboard::Up) ) {
                        game.controllingTank(*player2, direction_t::UP, speed);
                        if (Keyboard::isKeyPressed(Keyboard::Space) && past_shot.getElapsedTime().asMilliseconds() > 150) {
                            game.getTankOfPlayer2()->makeShot();
                            past_shot.restart();
                        }
                        isKeyPressedPlayer2 = true;
                    } else

                    if (Keyboard::isKeyPressed(Keyboard::Down)) {
                        game.controllingTank(*player2, direction_t::DOWN, speed);
                        if (Keyboard::isKeyPressed(Keyboard::Space) && past_shot.getElapsedTime().asMilliseconds() > 150) {
                            game.getTankOfPlayer2()->makeShot();
                            past_shot.restart();
                        }
                        isKeyPressedPlayer2 = true;
                    } else

                    if (Keyboard::isKeyPressed(Keyboard::Space) && past_shot.getElapsedTime().asMilliseconds() > 150) {
                        game.getTankOfPlayer2()->makeShot();
                        past_shot.restart();
                        isKeyPressedPlayer2 = true;
                    } else {
                        isKeyPressedPlayer2 = false;
                    }

                    if (isKeyPressedPlayer2)
                    {
                        float player2_x   = game.getTankOfPlayer2()->getPosition().x,
                              player2_y   = game.getTankOfPlayer2()->getPosition().y;
                        dir_t current_dir = game.getTankOfPlayer2()->getDirection();
                        uint8_t helth     = game.getTankOfPlayer2()->getHelth();
                        bool isShot = game.getTankOfPlayer2()->getListShots().size();
                        packet << player2_x << player2_y << static_cast<uint8_t>(current_dir) << helth << isShot;
                        socket.send(packet);
                    }

                }
                if (socket.receive(packet) == sf::Socket::Done){
                    float player1_x, player1_y;
                    uint8_t current_dir, helth;
                    bool isShot;
                    packet  >> player1_x >> player1_y >> current_dir >> helth >>isShot;
                    game.getTankOfPlayer1()->setPosition(player1_x, player1_y);
                    game.getTankOfPlayer1()->setDirection(static_cast<tank_t::direction_t>(current_dir));
                    game.getTankOfPlayer1()->getHelth() = helth;
                    if (isShot && past_shot_enemy.getElapsedTime().asMilliseconds() > 150) {
                        game.getTankOfPlayer1()->makeShot();
                        past_shot_enemy.restart();
                    }
                }
            }




//            game.isKeyboardControlling(game.getTankOfPlayer1(), 0.1f, p);
//            game.isKeyboardControlling(game.getTankOfPlayer2(), 0.1f, packet);


            for (auto& i : threads) {
                i.join();
            }
        }
            threads.clear();
            game.update(window);

    }
    return 0;
}

