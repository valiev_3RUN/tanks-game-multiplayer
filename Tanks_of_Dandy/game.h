#ifndef GAME_H
#define GAME_H
#include "common_params.h"
#include "map.h"
#include "tank.h"

class game_t
{
public:
    enum game_mode_t : uint8_t { SINGLE_PLAYER, MULTIPLAYER };
    enum current_type_connect_t : uint8_t { SERVER, CLIENT };
private:
    Text text;
    Clock clock, clock_shot;
    using    direction_t = entity_t::direction_t;
    float           time = 0;
    uint8_t   count_bots = 0,
         size_square_map = 0;
    tank_t *tank_player1 = nullptr,
           *tank_player2 = nullptr;
    map_t           *map = nullptr;
    vector<tank_t*> bots;
    enum game_mode_t game_mode;
    enum current_type_connect_t current_tp;


private:
    void createTexts();
    void update_times();
    void update_texts(RenderWindow& window);
    void update_tanks(RenderWindow& window);
public:
    tank_t player1,
           player2;
    string choose;
    game_t();
    explicit game_t(game_mode_t game_mode);
    ~game_t();
    bool createTcpServer(uint16_t port);
    bool createTcpClient(uint16_t port);
    void createMap(const string map_path, size_t size_square_map);
    void createPlayer1(const string tank_player1_path);
    void createPlayer2(const string tank_player2_path);
    void createBotsTank(const string bot_tank_path, uint8_t count);

    bool isHaveResolutionToMove(entity_t& entity, direction_t dir);
    bool controllingTank(tank_t& tank, direction_t dir, float speed);
    void moveBots();
    tank_t* getTankOfPlayer1();
    tank_t* getTankOfPlayer2();
    vector<tank_t *> &getTanksOfBots();
    void update(RenderWindow& window);
    void isKeyboardControlling(tank_t *tank, float speed, Packet &packet);

};



#endif // GAME_H
