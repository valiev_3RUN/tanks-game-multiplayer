#ifndef MAP_H
#define MAP_H
#include "common_params.h"
#include "tank.h"

class map_t {
private:
    Image map_tail_image;
    Texture map_texture;
    Sprite map_sprite;
    size_t size_square_map;
    const char* path;
    vector<Vector2f> coordinates_obstacles;
    std::string* arr_map_string = nullptr;

private:
    map_t(const string path);
public:
    friend class game_t;
    explicit map_t(size_t size_square_map, const string path);
    ~map_t();
    void draw(RenderWindow& window);
    Vector2u getSizeTailImage() const;
    vector<Vector2f> getCoordinatesOfObstacles() const;
    string& operator[] (uint32_t i);
   };

#endif // MAP_H


















