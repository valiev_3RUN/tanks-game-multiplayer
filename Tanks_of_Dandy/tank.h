#ifndef _CLASS_PLAYER_H
#define _CLASS_PLAYER_H
#include "common_params.h"

class entity_t {
public:
    enum direction_t : uint8_t { RIGHT = 0, LEFT, UP, DOWN };
    enum type_entity_t : uint8_t { PLAYER_1, PLAYER_2, BOT };
public:
    entity_t() {}
    entity_t(const string path);
    virtual ~entity_t();
    void     move(const float& x, const float& y);
    Vector2f getPosition() const;
    void     setPosition(const float& x,const  float& y);
    uint8_t& getHelth();
    uint8_t  getHight();
    Sprite&  getSprite();
    type_entity_t getTypeEntity();
    void     setDirection(direction_t direction);
    direction_t getDirection();
    Image&   getImage();
protected:
    enum direction_t current_dir;
    enum type_entity_t type_entity;
    Image   obj_image;
    Sprite  obj_sprite;
    Texture obj_texture;
    uint8_t helth;

};


class bullet_t : public entity_t {
private:
public:
    bullet_t();
    bullet_t(const string path);
    ~bullet_t() override;
};



class tank_t : public entity_t {
private:
public:
    tank_t() {}
    vector<bullet_t> list_shots;
    tank_t(const string path, type_entity_t type_entity);
    ~tank_t() override;
    void setDamage(uint8_t damage);
    void makeShot();
    vector<bullet_t>& getListShots();

};

#endif
