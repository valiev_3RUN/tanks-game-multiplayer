#include "map.h"


map_t::map_t(const string path) {
    this->map_tail_image.loadFromFile(path);
    this->map_texture.loadFromImage(this->map_tail_image);
    this->map_sprite.setTexture(this->map_texture);
}

map_t::map_t(size_t size_square_map, const string path) : map_t(path) {

    this->size_square_map = size_square_map;
    this->arr_map_string = new string[size_square_map];
    assert(size_square_map < UINT8_MAX);
    char arr_objects[] = {'0', ' ', ' ', ' ',' ',' ', ' ', ' ', 's'};
//    srand(static_cast<unsigned int>(time(nullptr)));
    srand(static_cast<unsigned int>(1));

    for (uint8_t i = 0; i < size_square_map; ++i) {
        for (uint8_t j = 0; j < size_square_map; ++j) {
            if (i == 0 || j == 0 || i == size_square_map -1 || j == size_square_map - 1) {
                arr_map_string[i].push_back('0');
            } else {
                if ((i == 1 && j == 1) || (i == 1 && j == size_square_map - 2) || (j == 1 && i == size_square_map - 2)) {
                  arr_map_string[i].push_back(' ');
                } else {
                    int index_arr_obj =  1 + rand() % (static_cast<int>(sizeof (arr_objects)) / static_cast<int>(sizeof (arr_objects[0]))-1);
                    arr_map_string[i].push_back(arr_objects[index_arr_obj]);
                }
            }
            switch (this->arr_map_string[i][j]) {
            case 's':
            case '0':
//                float size_w_h = static_cast<float>(this->map_tail_image.getSize().y);
                this->coordinates_obstacles.push_back( {static_cast<float>(j*map_tail_image.getSize().y), static_cast<float>(i*map_tail_image.getSize().y)});
                break;
            }
        }
    }
}

map_t::~map_t() {
    if (this->arr_map_string != nullptr) delete[] this->arr_map_string;
}
string& map_t::operator[](uint32_t i) {
    return this->arr_map_string[i];
}

Vector2u map_t::getSizeTailImage() const {
    return this->map_tail_image.getSize();
}

vector<Vector2f> map_t::getCoordinatesOfObstacles() const {
    return this->coordinates_obstacles;
}

void map_t::draw(RenderWindow& window) {
    int map_tail_w_h = static_cast<int>(this->map_tail_image.getSize().y);
    for (uint8_t i = 0; i < this->size_square_map; ++i) {
        for (uint8_t j = 0; j < this->size_square_map; ++j) {
            switch (this->arr_map_string[i][j]) {
            case ' ':
                this->map_sprite.setTextureRect(IntRect(0,0,map_tail_w_h,map_tail_w_h));
                break;
            case 's':
                this->map_sprite.setTextureRect(IntRect(map_tail_w_h,0,map_tail_w_h,map_tail_w_h));
                break;
            case '0':
                this->map_sprite.setTextureRect(IntRect(map_tail_w_h*2,0,map_tail_w_h,map_tail_w_h));
                break;
            case 'h':
                this->map_sprite.setTextureRect(IntRect(map_tail_w_h*4,0,map_tail_w_h,map_tail_w_h));
                break;
            }
            this->map_sprite.setPosition(j*this->map_tail_image.getSize().y, i*this->map_tail_image.getSize().y);
            window.draw(this->map_sprite);
        }
    }
}
