TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lsfml-graphics -lsfml-window -lsfml-system  -lsfml-network -pthread
SOURCES += \
        main.cpp \
    tank.cpp \
    game.cpp \
    map.cpp

HEADERS += \
    tank.h \
    map.h \
    common_params.h \
    game.h
